document.querySelector("#archivo1").addEventListener
    ("change", leerArchivo, false);
function leerArchivo(e) {
    let archivo = e.target.files[0];
    if (!archivo) {
        return;
    }
    let lector = new FileReader(); //API DE JS QUE VA A LEER EL CONTENIDO 
    //creamo un obejeto que se llama lector.
    lector.readAsText(archivo);
    lector.onload = function (e) {
        let contenido = e.target.result;
        mostrarContenido(contenido);
    }
}

function mostrarContenido(contenido) {
    let parrafo = document.querySelector("#parrafo");
    parrafo.innerHTML = contenido;
}





//PROCESO PARA LECTURA DE ARCHIVO CSV
document.querySelector("#archivo2").addEventListener
    ("change", leerArchivo2, false);

function leerArchivo2(e) {
    let archivo = e.target.files[0];
    if (!archivo) {
        return;
    }
    let lector = new FileReader(); //API DE JS QUE VA A LEER EL CONTENIDO 
    //creamo un obejeto que se llama lector.
    lector.readAsText(archivo);
    lector.onload = function (e) {
        let contenido = e.target.result;
        mostrarTabla(contenido);
    }
}

function mostrarTabla(contenido) {
    let tabular = document.querySelector("#tabular");
    let filas = contenido.split(/\r?\n|\r/);
    let template = `<table>`;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        //console.log (celdasFila[0]," - ",celdasFila[1]," - ",celdasFila[2]," - ",celdasFila[3]," - ")
        if (i == 0) {
            template += `<tr><th>${celdasFila[0]}</th><th>${celdasFila[1]}</th><th>${celdasFila[2]}</th><th>${celdasFila[3]}</th></tr>`
        } else {
            template += `<tr><td>${celdasFila[0]}</td><td>${celdasFila[1]}</td><td>${celdasFila[2]}</td><td><img src="image/${celdasFila[3]}" >
      </td></tr>`


        }

    }
    template += `</table>`;
    tabular.innerHTML = template;
}

//PROCESO PARA EXPORTAR TEXTO A ARCHIVO

document.querySelector("#exportar").setAttribute("onclick", "exportarArchivo()");

function exportarArchivo() {
    let texto = document.querySelector("#texto").value;
    let nombreArchivo = "textoDeGestordeArchivo.txt";
    //console.log("Estoy en exportar");
    blob = new Blob([texto], { type: "octet/stream" });
    url = window.URL.createObjectURL(blob);
    //CREAR UN ELEMENTO DE ENLACE
    let a = document.createElement("a");
    a.href = url;
    a.download = nombreArchivo;
    a.click();
    window.URL.revokeObjectURL(url);
}


